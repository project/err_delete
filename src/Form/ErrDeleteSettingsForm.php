<?php

namespace Drupal\err_delete\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class ErrDeleteSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'err_delete.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'err_delete_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['remove_delete'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Remove original Delete button'),
      '#default_value' => $config->get('replace_delete'),
    ];
    $form['recursive_delete_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Recursive Delete button label'),
      '#default_value' => $config->get('replace_delete_label'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $editable_config = $this->configFactory->getEditable(static::SETTINGS);
    // Set the submitted configuration setting.
    $editable_config
      ->set('replace_delete', $form_state->getValue('remove_delete'))
      ->set('replace_delete_label', $form_state->getValue('recursive_delete_label'));
    $editable_config->save();

    parent::submitForm($form, $form_state);
  }

}
