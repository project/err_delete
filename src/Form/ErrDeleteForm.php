<?php

namespace Drupal\err_delete\Form;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class ErrDeleteForm extends FormBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The immutable entity clone settings configuration entity.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The editable entity clone settings configuration entity.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $editableConfig;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'err_delete_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param EntityInterface $node
   */
  public function buildForm(array $form, FormStateInterface $form_state, EntityInterface $node = NULL) {
    if (isset($node)) {
      $form = array_merge($form, getFormElement($node));

      $form['delete_node_id'] = [
        '#type' => 'hidden',
        '#value' => $node->id(),
      ];
    }

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['delete'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => t('Delete'),
    ];

    return ($form);
  }

  /**
   * {@inheritdoc}
   *
   * @filesource
   *
   * @return void
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->cleanValues()->getValues();
    $node_to_delete = $form_state->getValue('delete_node_id');

    deleteRefEntities($values);

    $node = \Drupal::entityTypeManager()->getStorage('node')->load($node_to_delete);

    if ($node) {
      $node->delete();
    }

    $dest_url = '/admin/content';
    $url = Url::fromUri('internal:' . $dest_url);
    $form_state->setRedirectUrl($url);
  }
}
