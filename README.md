# Entity Reference Recursive Delete | ERR Delete

Gives an alternative delete button and delete form for every node,
where you can select referenced entities to remove alongside with the node itself.
This module only triggers when there's at least one entity reference in the node,
and the current user has permission to use ERR Delete.

## INSTALLATION

Install as you would normally install a contributed Drupal module. Visit:
<https://www.drupal.org/node/1897420> for further information.

## MAINTAINERS

- ornagy - <https://www.drupal.org/u/ornagy>
- prompt-h - <https://www.drupal.org/u/prompt-h>
- BonkPrmpt - <https://www.drupal.org/u/bonkprmpt>
